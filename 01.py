import os

from dotenv import load_dotenv
from minio import Minio

if __name__ == "__main__":
    load_dotenv()

    client = Minio(
        os.getenv("MINIO_ENDPOINT"),
        access_key=os.getenv("MINIO_ACCESS_KEY"),
        secret_key=os.getenv("MINIO_SECRET_KEY"),
        secure=False,
    )
    # print(client)

    # Download the file:
    # https://min.io/docs/minio/linux/developers/python/API.html#fget_object
    # client.fget_object("test", "test.csv", "test.csv")

    try:
        # https://min.io/docs/minio/linux/developers/python/API.html#get_object
        # https://github.com/minio/minio-py/blob/7.1.17/minio/api.py#L1093
        # https://github.com/minio/minio-py/blob/7.1.17/minio/api.py#L406
        response = client.get_object("test", "test.csv")

        # print(response.auto_close)
        # response.auto_close = False
        # for line in io.TextIOWrapper(response):
        #     print(line)

        # https://urllib3.readthedocs.io/en/latest/index.html
        # https://urllib3.readthedocs.io/en/latest/user-guide.html
        # https://urllib3.readthedocs.io/en/latest/reference/urllib3.response.html#urllib3.response.HTTPResponse
        # print(response)
        # https://urllib3.readthedocs.io/en/latest/reference/urllib3.response.html#urllib3.response.HTTPResponse.data
        # https://urllib3.readthedocs.io/en/latest/advanced-usage.html#stream
        # https://urllib3.readthedocs.io/en/latest/user-guide.html#using-io-wrappers-with-response-content
        # https://docs.python.org/3/library/csv.html#csv.reader
        print(response.data)
        # print(type(response.data))
        # print(response.read())
    finally:
        response.close()
        response.release_conn()
