# Notes

- https://fly.io/docs/app-guides/minio/
- https://min.io/docs/minio/container/index.html
- https://min.io/docs/minio/macos/index.html
- https://github.com/minio/minio
- https://min.io/docs/minio/linux/developers/python/API.html
- https://min.io/docs/minio/linux/developers/python/minio-py.html
- https://github.com/minio/minio-py/blob/master/examples/get_object.py
- https://pypi.org/project/minio/
- https://stackoverflow.com/questions/71123560/getting-ssl-wrong-version-number-wrong-version-number-when-working-with-mini

## Commands

```bash
pipenv --python $(cat .python-version)
```

```bash
pipenv install --skip-lock python-dotenv==1.0.0 minio==7.1.17 && pipenv install --dev --skip-lock black==23.10.1
```

```bash
open ~/minio/data/
```
